#include <stdio.h>
#include <strings.h> 
#include <iostream> 
#include <netdb.h>
#include <sys/socket.h> 
#include <sys/types.h>
#include <sys/uio.h>
#include <netinet/tcp.h>
#include <unistd.h> 
#include <arpa/inet.h> 
#include <fstream> 
#include <vector>

using namespace std; 

int main(int argc, char* argv[])
{

if(argc != 2)
{
	cout << "USAGE: CLIENT SERVERIP" << endl;
	return -1; 
}

string website = argv[1]; 

cout << "The website is: "<< website << endl; 

int slash_index = 0;
for (int i = 0; i < website.size(); i++)
{
	if(website.at(i) == '/')
	{
		slash_index = i; 
		break; 
	}
}

cout << "The index of the slash is: " << slash_index << endl;

string domain_name = website.substr(0,slash_index); 
string file_path = website.substr(slash_index, website.size()); 

cout << "Domain Name: " << domain_name << "\nFile Path: " << file_path << endl;

string http_message = "GET " + file_path + " HTTP/1.1\r\n" + "HOST: " + 
domain_name + "\r\n\r\n";

cout << "\nThe HTTP message\n" << http_message << endl;

struct hostent *host = gethostbyname(domain_name.c_str()); //DNS lookup

if(host == NULL) 
{
	cout << "Couldn't resolve hostname " << endl;
	return -1; 
}
else
{
	cout << "DNS resolved" << endl;
}

int HTTPport = 80; 

/*Declaring structure of the socket*/ 
sockaddr_in sendSockAddr; 
bzero( (char*)&sendSockAddr, sizeof(sendSockAddr)); 
sendSockAddr.sin_family = AF_INET; 
sendSockAddr.sin_addr.s_addr = 
	inet_addr(inet_ntoa(*(struct in_addr*)*host->h_addr_list));
sendSockAddr.sin_port = htons(HTTPport);

int sd = socket(AF_INET, SOCK_STREAM,0);

if (sd == -1)
{
	cout << "Error occured in socket creation" << endl;
} 
else
{
	cout << "Socket Created" << endl;
}

int connection = connect(sd, (sockaddr*)&sendSockAddr, sizeof(sendSockAddr)); 

char *buffer;
buffer = new char [1500]; 


//vector<char> vecbuffer; 

cout << "CONNECT: " << connection << endl; 

if(connection == -1)
	cout << "Error connect function returned an error" << endl; 
else
	cout << "Socket connected" << endl;

const char *temp = http_message.c_str(); 

int result = send(sd, temp, strlen(temp), 0); //strlen(temp) 

cout << "RESULT: "<< result << endl;

cout << "sizeof: " << sizeof(http_message) << endl;

int bytesread = read(sd, buffer, 1500); 

cout << "\nContents of the message:\n" << endl;

ofstream outputFile; 
outputFile.open("WebsiteOut.txt");
 
for (int i = 0; i < 1500; i++)
{
	outputFile << buffer[i]; 
	cout << buffer[i];
/*
	if (buffer[i] == "<")
	{
		for (int j = i; buffer[j] == " "; j--)
		{
			string message_length = buffer[j];
		}
	}
*/
}

cout <<"\nBytes read: " << bytesread << endl;

char *response; 

response = new char[1500]; 

recv(sd, response, 1500,0);

cout << response; 




return 0; 
}
