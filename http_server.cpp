    #include <stdlib.h>
    #include <sys/types.h>    // socket, bind
    #include <sys/socket.h>   // socket, bind, listen, inet_ntoa
    #include <netinet/in.h>   // htonl, htons, inet_ntoa
    #include <arpa/inet.h>    // inet_ntoa
    #include <iostream>        // gethostbyname
    #include <unistd.h>        // read, write, close
    #include <strings.h>       // bzero
    #include <netinet/tcp.h>  // SO_REUSEADDR
    #include <sys/uio.h>      // writev
    #include <pthread.h>
    #include <sys/time.h> 
    #include <sstream>
    #include <string>
    #include <fstream>
//he

using namespace std;

void *thread_server (void *data_struct);

string getFile(string); 
struct thread_data
{
	int repetition;
	int sd;
};

int main(int argc, char* argv[])
{

if (argc != 3)
{
	cout << "USAGE: CLIENT PORT REPETITION\nNeeds to be 3 args, Actaully: " << argc << endl;
	for (int i = 0; i < argc; i++)
		cout << "argv[" << i << "] = " << argv[i] << endl;
	return -1;
}
int port = atoi(argv[1]);  // the last 4 digits of your student id
int MAX_CONNECTIONS = 1024;
int repetition = atoi(argv[2]);

    sockaddr_in acceptSockAddr;
    bzero( (char*)&acceptSockAddr, sizeof( acceptSockAddr ) );
    acceptSockAddr.sin_family      = AF_INET; // Address Family Internet
    acceptSockAddr.sin_addr.s_addr = htonl( INADDR_ANY );
    acceptSockAddr.sin_port        = htons( port );

	cout << "Creating Socket" << endl;

    int serverSd = socket( AF_INET, SOCK_STREAM, 0 );

	cout << "Setting Socket Port" << endl;
    const int on = 1;
    setsockopt( serverSd, SOL_SOCKET, SO_REUSEADDR, (char *)&on,
                sizeof( int ) );
	cout << "Binding Socket" << endl;
	bind( serverSd, ( sockaddr* )&acceptSockAddr, sizeof( acceptSockAddr ) );


    listen( serverSd, 1);

    sockaddr_in newSockAddr;
    socklen_t newSockAddrSize = sizeof( newSockAddr );

    cout << "Waiting for connection" << endl;


    int newSd = accept( serverSd, ( sockaddr *)&newSockAddr, &newSockAddrSize);
	cout << "After the accept" << endl;

    //pthread_t thread;
    //struct thread_data *data = new thread_data; // will be deleted by thread

    //data->repetition = repetition;
    //data->sd = newSd;
    //int iret = pthread_create(&thread, NULL, thread_server, (void*)data);
    //if(iret)
	//cout << "iret Error " << endl;
    //else
	//cout << "thread created" << endl;

	char * response;
	response = new char[1500]; 

	recv(newSd, response, 1500, 0); 


	char* parseReq = strtok(response, " ");

	if (strcmp(parseReq, "GET") != 0)
	{
		//send bad request back
	
	} 

	parseReq = strtok(NULL, " "); 
	string filepath = parseReq;
	cout <<"filepath: "<< filepath;
	cout << response; 

	string response1(response); 

	cout << "response1: " << response1 << endl;

//	for (int i = 0; i < strlen(response1); i++)
//	{
//		if(response1.at[i] == " ")
//			
//			for (int j = i; j < response1.strlen(); j++)
//			{
//				filepath.at(i) = response1.at(i);
//				if (response.at(i) == " ")
//					break; 
//			}
//		break;
//	}

//	cout << "Filepath: " << filepath << endl;

	ofstream file; 
	file.open(filepath);
//	if (!file.is_open())	
//	{
//		cout << "404" << endl;
//	}

//	char *sendback;
//	sendback = new char[1500]; 
//	char c;	
	for (int i = 0; i < 1500; i++)
	{

//		 file.fgetc(c);
//		sendback[i] = c;  
	
	}
	string sendback;
	
	string filename = filepath.substr(1, filepath.size());
	
	cout << "filename: " << filename << endl;
		
	sendback = getFile(filename); 

	if (sendback.length() == 0)
	{
		sendback = getFile("404.txt"); 				
	}

	if (filename.compare("secretfile.txt") == true)
	{
		sendback = getFile("secretfile.txt");
		
	}
	
	if (filepath.find("../") != -1)
	{
		sendback = getFile("403.txt");
	}

	cout << "sendback " << sendback << endl;

	send(newSd, sendback.c_str(), sendback.length(), 0); 

	//cout << "sendresult " << sendresult << endl;

	
	

return 0;
}

string getFile(string fileName) {
    stringstream fdata;
    ifstream myfile;
    string line;
    myfile.open(fileName.c_str(), ios_base::in);
    if (myfile.is_open()) {
        while (myfile.good()) {
            getline(myfile, line);
            fdata << line << "\n";
        }
    }
	else 
		cout << "file is not open 404" << endl;
    myfile.close();
    return fdata.str();
}
